package main

import "gitlab.oit.duke.edu/devil-ops/packratking/packratking/cmd"

func main() {
	cmd.Execute()
}
