package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getVolumesCmd represents the getVolumes command
var getVolumesCmd = &cobra.Command{
	Use:     "volumes",
	Short:   "Get location information",
	Long:    `Get information about volumes in Packrat`,
	Aliases: []string{"volume", "v"},
	Args:    cobra.RangeArgs(0, 1),
	Example: `$ packratking get volumes
## Get by ID
$ packratking get volumes 42
## Get by Name
$ packratking get volumes some-volume`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			r, _, err := client.Volume.List(context.Background())
			if err != nil {
				return err
			}
			gout.MustPrint(r)
			logger.Info("returned items", "count", len(r))
		} else {
			r, _, err := client.Volume.Get(context.Background(), args[0])
			if err != nil {
				return err
			}
			gout.MustPrint(r)
		}
		return nil
	},
}

func init() {
	getCmd.AddCommand(getVolumesCmd)
}
