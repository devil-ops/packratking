package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"
)

// createNFSCmd represents the createNFS command
var createNFSCmd = &cobra.Command{
	Use:     "nfs",
	Short:   "Create NFS Volume",
	Long:    `Create a new NFS Volume in Packrat`,
	Example: `$ packratking create nfs -f 000-9332 -r 10.138.3.167/32 -n oit-nas-fe11.oit.duke.edu my-super-sweet-share`,
	Args:    cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		size := mustGetCmd[float64](*cmd, "size-gb")
		rootCidr := mustGetCmd[string](*cmd, "root-cidr")
		fundCode := mustGetCmd[string](*cmd, "fund-code")
		nasDevice := mustGetCmd[string](*cmd, "nas-device")
		ctx := context.Background()

		location, _, err := client.Location.GetLocationByFQDN(ctx, nasDevice)
		if err != nil {
			return err
		}

		params := &packrat.VolumeCreateParams{
			FundCode:   fundCode,
			LocationID: location.ID,
			Name:       args[0],
			RequestedState: packrat.RequestedState{
				MaxGB: size,
				NFSExportSpec: []packrat.NFSExportSpec{
					{
						Address: rootCidr,
						Type:    "Root",
					},
				},
				ShareNFS: toPTR(true),
			},
		}
		gout.MustPrint(params)
		res, _, err := client.Volume.Create(ctx, params)
		if err != nil {
			return err
		}
		gout.MustPrint(res)
		return nil
	},
}

func init() {
	createCmd.AddCommand(createNFSCmd)
	createNFSCmd.PersistentFlags().StringP("root-cidr", "r", "", "CIDR to export Root NFS to")
	checkErr(createNFSCmd.MarkPersistentFlagRequired("root-cidr"))
}
