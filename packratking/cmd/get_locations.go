package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getLocationsCmd represents the getLocations command
var getLocationsCmd = &cobra.Command{
	Use:     "locations",
	Short:   "Get location information",
	Aliases: []string{"location", "l"},
	Example: `$ packratking get location`,
	Long:    `Get information about locations in Packrat`,
	RunE: func(cmd *cobra.Command, args []string) error {
		r, _, err := client.Location.GetLocations(context.Background())
		if err != nil {
			return err
		}
		gout.MustPrint(r)
		logger.Info("returned items", "count", len(r))
		return nil
	},
}

func init() {
	getCmd.AddCommand(getLocationsCmd)
}
