/*
Package cmd is the command line application for packratking 🐀
*/
package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"strings"

	"github.com/charmbracelet/log"

	"github.com/drewstinnett/gout/v2"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"

	"github.com/spf13/viper"
)

var (
	cfgFile string
	client  *packrat.Client
	version string = "dev"
	logger  *slog.Logger
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:           "packratking",
	Short:         "Interact with Packrat from the CLI",
	Version:       version,
	SilenceUsage:  true,
	SilenceErrors: true,
	Long: `Interface with packrat from the command line! Be sure to get your long lived
token from the fine folks in IDMS. For information on how to do that, see: 

https://packrat.oit.duke.edu/api/v2/docs

Your token can either be used with any of the methods below:

* Passed to packratking with '-l' or '--long-lived-token'
* Set in the environment variable: IDMSOIDC_LONG_LIVED_TOKEN
* Stored in ~/.packratking.yaml as 'long-lived-token'`,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		checkErr(gout.WithCobra(cmd, nil))
		logger = slog.New(log.NewWithOptions(os.Stderr, log.Options{
			Prefix: "🐀 packrat 👑",
		}))
		llt := viper.GetString("long-lived-token")
		if llt == "" {
			log.Fatal("Must set long lived token first")
		}
		var err error
		client, err = packrat.New(packrat.WithLLT(llt))
		return err
	},
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.packratking.yaml)")
	rootCmd.PersistentFlags().StringP("long-lived-token", "l", "", "Long Lived Token (Can also be passed as env IDMSOIDC_LONG_LIVED_TOKEN)")
	if err := viper.BindPFlag("long-lived-token", rootCmd.PersistentFlags().Lookup("long-lived-token")); err != nil {
		panic(err)
	}

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	checkErr(gout.BindCobra(rootCmd, nil))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".packratking" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".packratking")
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("IDMSOIDC")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
