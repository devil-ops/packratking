package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create NFS or SMB Volumes",
	Long:  `Create NFS or SMB Volumes using Packrat`,
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		return fmt.Errorf("not a valid subommand: %v", args[0])
	},
}

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.PersistentFlags().Float64P("size-gb", "s", 100, "Size in GB of new share")
	createCmd.PersistentFlags().StringP("fund-code", "f", "", "Fund code to use for billing")
	checkErr(createCmd.MarkPersistentFlagRequired("fund-code"))
	createCmd.PersistentFlags().StringP("nas-device", "n", "", "FQDN of the NAS Device Location")
	checkErr(createCmd.MarkPersistentFlagRequired("nas-device"))
}
