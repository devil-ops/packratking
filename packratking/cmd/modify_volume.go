package cmd

import (
	"context"
	"encoding/json"
	"os"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"
)

// modifyVolumeCmd represents the modifyVolume command
var modifyVolumeCmd = &cobra.Command{
	Use:   "volume ID",
	Short: "Modify a volume",
	Args:  cobra.ExactArgs(1),
	Long:  `Modify a packrat volume using the API`,
	Run: func(cmd *cobra.Command, args []string) {
		// id, err := strconv.Atoi(args[0])
		// checkErr(err)
		v, _, err := client.Volume.Get(context.Background(), args[0])
		checkErr(err)

		var hasChanges bool
		changes := &packrat.VolumeModifyParams{
			RequestedState: packrat.RequestedState{},
		}

		if cmd.PersistentFlags().Changed("size") {
			hasChanges = true
			changes.RequestedState.MaxGB = mustGetCmd[float64](*cmd, "size")
		}
		if cmd.PersistentFlags().Changed("fund-code") {
			hasChanges = true
			changes.FundCode = mustGetCmd[string](*cmd, "fund-code")
		}
		if cmd.PersistentFlags().Changed("enable-nfs") {
			hasChanges = true
			changes.RequestedState.ShareNFS = toPTR(mustGetCmd[bool](*cmd, "enable-nfs"))
		}
		if cmd.PersistentFlags().Changed("enable-smb") {
			hasChanges = true
			changes.RequestedState.ShareSMB = toPTR(mustGetCmd[bool](*cmd, "enable-smb"))
		}
		if cmd.PersistentFlags().Changed("export-json") {
			hasChanges = true
			var vol packrat.Volume
			uerr := json.Unmarshal([]byte(mustGetCmd[string](*cmd, "export-json")), &vol)
			checkErr(uerr)
			changes.RequestedState.NFSExportSpec = vol.ExportAddresses

		}
		if cmd.PersistentFlags().Changed("export") {
			hasChanges = true
			exports := mustGetCmd[[]string](*cmd, "export")
			specs := make([]packrat.NFSExportSpec, len(exports))
			for idx, exportS := range exports {
				export, eerr := packrat.NewExportSpec(exportS)
				checkErr(eerr)
				specs[idx] = *export
			}
			changes.RequestedState.NFSExportSpec = specs
		}

		if !hasChanges {
			logger.Error("no changes to make")
			os.Exit(2)
		}

		logger.Info("requesting the following changes")
		gout.MustPrint(changes)

		res, _, err := client.Volume.Modify(context.Background(), v.ID, changes)
		checkErr(err)
		logger.Info("volume now has the following attributes")
		gout.MustPrint(res)
	},
}

func init() {
	modifyCmd.AddCommand(modifyVolumeCmd)
	modifyVolumeCmd.PersistentFlags().Float64P("size", "s", 0, "New size of Volume")
	modifyVolumeCmd.PersistentFlags().StringP("fund-code", "f", "", "Fund code for billing")
	modifyVolumeCmd.PersistentFlags().BoolP("enable-smb", "m", false, "Enable SMB Sharing")
	modifyVolumeCmd.PersistentFlags().BoolP("enable-nfs", "n", false, "Enable NFS Sharing")
	modifyVolumeCmd.PersistentFlags().String("export-json", "", `Edit exports using a json blob of text.
Example:
  --export-json '
  {
	"export_addresses": [
	  {
	    "address": "1.2.3.4/24",
	    "type": "Root"
	  },
	  {
	    "address": "4.3.2.1",
	    "type": "Read/Write"
	  }
	]
  }
'

This method may be expanded later to modify more than just the exports, which is why you need the "export_addresses" key for now.`)
	modifyVolumeCmd.PersistentFlags().StringSliceP("export", "e", []string{}, `NFS Exports, in the form of 'type:cidr'.
Examples:
  --export root:1.2.3.4
  --export rw:1.2.3.4
  --export ro:1.2.3.4

This can be specified multiple times.

⚠️ this will remove any exports you have not specified ⚠️`)

	modifyVolumeCmd.MarkFlagsMutuallyExclusive("export", "export-json")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// modifyVolumeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
