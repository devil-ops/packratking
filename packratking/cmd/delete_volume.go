package cmd

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

// deleteVolumeCmd represents the deleteVolume command
var deleteVolumeCmd = &cobra.Command{
	Use:   "volume ID",
	Short: "Delete a volume from Packrat",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		ctx := cmd.Context()
		id, err := strconv.Atoi(args[0])
		checkErr(err)
		volume, _, err := client.Volume.GetWithID(ctx, id)
		checkErr(err)
		fmt.Printf("Are you sure you want to delete: '%s' (y or n)\n", volume.Name)
		confirm := askForConfirmation()
		if !confirm {
			return errors.New("quitting without doing the delete")
		}
		logger.Info("Deleting volume", "id", volume.ID)
		_, err = client.Volume.Delete(ctx, id)
		if err != nil {
			return fmt.Errorf("error delting volume: %v", err)
		}
		logger.Info("Deleted volume! ☠️☠️☠️")
		return nil
	},
}

func init() {
	deleteCmd.AddCommand(deleteVolumeCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteVolumeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteVolumeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
