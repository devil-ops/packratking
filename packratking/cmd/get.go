package cmd

import (
	"errors"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get items from packrat",
	Long:  `Get items from packrat`,
	Args:  cobra.ExactArgs(1),
	PreRunE: func(cmd *cobra.Command, args []string) error {
		llt := viper.GetString("long-lived-token")
		if llt == "" {
			return errors.New("set your long lived token first please")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		return errors.New(cmd.UsageString())
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// getCmd.PersistentFlags().StringP("output", "o", "yaml", "Output format.  One of yaml|json|plain|gotemplate|tsv")
	// getCmd.PersistentFlags().String("output-template", "", "Template for output format, if available")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
