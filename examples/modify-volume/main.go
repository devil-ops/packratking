package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"
)

func main() {
	llToken := os.Getenv("IDMSOIDC_LONG_LIVED_TOKEN")

	if len(os.Args) != 2 {
		log.Fatalf("Usage %s VOLUME_ID", os.Args[0])
	}

	if llToken == "" {
		log.Fatal("Please set you long lived token in IDMSOIDC_LONG_LIVED_TOKEN")
	}

	ratking := packrat.MustNew(packrat.WithLLT(llToken))
	ctx := context.Background()

	volId, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	changeset := packrat.VolumeModifyParams{
		RequestedState: packrat.RequestedState{
			MaxGB: 210,
		},
	}

	res, _, err := ratking.Volume.Modify(ctx, float64(volId), &changeset)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Result: ", res)
}
