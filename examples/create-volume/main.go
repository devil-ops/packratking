package main

import (
	"context"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"
)

func main() {
	llToken := os.Getenv("IDMSOIDC_LONG_LIVED_TOKEN")

	if llToken == "" {
		log.Fatal("Please set you long lived token in IDMSOIDC_LONG_LIVED_TOKEN")
	}

	ratking, err := packrat.New(packrat.WithLLT(llToken))
	if err != nil {
		panic(err)
	}
	ctx := context.Background()

	exportSpec := &packrat.NFSExportSpec{
		Address: "10.138.3.167/32",
		Type:    "Root",
	}
	truthy := true
	state := &packrat.RequestedState{
		MaxGB: 101,
		NFSExportSpec: []packrat.NFSExportSpec{
			*exportSpec,
		},
		ShareNFS: &truthy,
	}

	params := &packrat.VolumeCreateParams{
		FundCode:       "000-9332",
		LocationID:     7,
		Name:           "001-packratking-test",
		RequestedState: *state,
	}
	res, _, err := ratking.Volume.Create(ctx, params)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Result: ", res)
}
