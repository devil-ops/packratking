package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/packratking/packrat"
)

func main() {
	llToken := os.Getenv("IDMSOIDC_LONG_LIVED_TOKEN")

	if llToken == "" {
		log.Fatal("Please set you long lived token in IDMSOIDC_LONG_LIVED_TOKEN")
	}

	ratking := packrat.MustNew(packrat.WithLLT(llToken))
	ctx := context.Background()
	volumes, _, err := ratking.Volume.List(ctx)
	if err != nil {
		log.Fatal(err)
	}
	for _, volume := range volumes {
		fmt.Printf("%+v\n", volume)
	}
}
