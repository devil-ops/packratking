package packrat

import (
	"context"
	"fmt"
	"net/http"
)

const (
	locationPath = "/api/v2/locations"
)

// Location is generally a storage array or appliance
type Location struct {
	Active  bool `json:"active,omitempty"`
	APIData struct {
		Zone string `json:"zone,omitempty"`
	} `json:"api_data,omitempty"`
	APIEndpointAddress        string      `json:"api_endpoint_address,omitempty"`
	APIEndpointID             float64     `json:"api_endpoint_id,omitempty"`
	APIParentObjectIdentifier *string     `json:"api_parent_object_identifier,omitempty"`
	BackupManagerID           *string     `json:"backup_manager_id,omitempty"`
	BackupType                string      `json:"backup_type,omitempty"`
	BackupsOffSiteRetention   float64     `json:"backups_off_site_retention,omitempty"`
	BackupsOnSiteRetention    float64     `json:"backups_on_site_retention,omitempty"`
	BillingRateID             *int        `json:"billing_rate_id,omitempty"`
	CmdbUUID                  string      `json:"cmdb_uuid,omitempty"`
	Cnames                    *[]string   `json:"cnames,omitempty"`
	Container                 string      `json:"container,omitempty"`
	CreatedAt                 string      `json:"created_at,omitempty"`
	Fqdn                      string      `json:"fqdn,omitempty"`
	ID                        float64     `json:"id,omitempty"`
	MaxGb                     float64     `json:"max_gb,omitempty"`
	MinGb                     float64     `json:"min_gb,omitempty"`
	Name                      string      `json:"name,omitempty"`
	Restricted                bool        `json:"restricted,omitempty"`
	Secure                    bool        `json:"secure,omitempty"`
	SnapshotRetentionDays     float64     `json:"snapshot_retention_days,omitempty"`
	SysadminGrouperGroupID    *int        `json:"sysadmin_grouper_group_id,omitempty"`
	System                    bool        `json:"system,omitempty"`
	Type                      interface{} `json:"type,omitempty"`
	UpdatedAt                 *string     `json:"updated_at,omitempty"`
}

// LocationService is an interface for interfacing with the the location endpoint
type LocationService interface {
	GetLocations(ctx context.Context) ([]Location, *Response, error)
	GetLocationByFQDN(ctx context.Context, fqdn string) (*Location, *Response, error)
}

// LocationServiceOp is the Location Service Operator
type LocationServiceOp struct {
	client *Client
}

// GetLocationByFQDN returns a location using the fqdn
func (svc *LocationServiceOp) GetLocationByFQDN(ctx context.Context, fqdn string) (*Location, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, locationPath), nil)
	if err != nil {
		return nil, nil, err
	}
	var locations []Location
	var matchingLocations []Location
	_, err = svc.client.sendRequest(req, &locations)
	if err != nil {
		return nil, nil, err
	}
	for _, location := range locations {
		if location.Fqdn == fqdn {
			matchingLocations = append(matchingLocations, location)
		}
	}
	switch {
	case len(matchingLocations) > 1:
		return nil, nil, fmt.Errorf("found more than 1 location with fqdn of %s", fqdn)
	case len(matchingLocations) == 0:
		return nil, nil, fmt.Errorf("found no locations with fqdn of %s", fqdn)
	default:
		return &matchingLocations[0], nil, nil
	}
}

// GetLocations returns all locations
func (svc *LocationServiceOp) GetLocations(ctx context.Context) ([]Location, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, locationPath), nil)
	if err != nil {
		return nil, nil, err
	}
	var locations []Location
	_, err = svc.client.sendRequest(req, &locations)
	if err != nil {
		return nil, nil, err
	}
	return locations, nil, nil
}
