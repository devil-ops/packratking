package packrat

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVolumeGet(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		testCopyFile("./testdata/volumes.json", w)
	}))
	c := MustNew(WithBaseURL(srv.URL), WithSLT("fake"))
	require.NotNil(t, c)
	got, _, err := c.Volume.Get(context.TODO(), "3036")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, 3, len(got.ExportAddresses))
}

func TestNewExportAddress(t *testing.T) {
	tests := map[string]struct {
		given   string
		expect  NFSExportSpec
		wantErr string
	}{
		"default-root": {
			given:  "1.2.3.4",
			expect: NFSExportSpec{Type: "Root", Address: "1.2.3.4"},
		},
		"too-many-colons": {
			given:   "ro:1.2.3.4:ro",
			wantErr: "must use a single :, not multiples",
		},
		"invalid-type": {
			given:   "foo:1.2.3.4",
			wantErr: "no export type for: foo",
		},
		"read-only": {
			given:  "ro:1.2.3.4",
			expect: NFSExportSpec{Type: "Read Only", Address: "1.2.3.4"},
		},
	}
	for desc, tt := range tests {
		got, err := NewExportSpec(tt.given)
		if tt.wantErr != "" {
			require.EqualError(t, err, tt.wantErr, desc)
		} else {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, *got, desc)
		}
	}
}

func TestExportType(t *testing.T) {
	tests := map[string]struct {
		given   string
		expect  string
		wantErr string
	}{
		"root": {
			given:  "root",
			expect: "Root",
		},
		"bad type": {
			given:   "bad-type",
			wantErr: "no export type for: bad-type",
		},
	}
	for desc, tt := range tests {
		got, err := exportTypeWithString(tt.given)
		if tt.wantErr != "" {
			require.EqualError(t, err, tt.wantErr, desc)
			require.Equal(t, "", got, desc)
		} else {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, got)
		}
	}
}
