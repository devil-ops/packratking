/*
Package packrat interacts with the PackRat API
*/
package packrat

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-iam-oidc/authenticate"
	"moul.io/http2curl/v2"
)

const (
	baseURL = "https://packrat.oit.duke.edu"
)

// Client is the thing that interacts with the API
type Client struct {
	client    *http.Client
	userAgent string
	printCurl bool
	Config    ClientConfig
	llt       string // long lived token
	slt       string // short lived token
	baseURL   string
	Location  LocationService
	Volume    VolumeService
}

// ClientConfig configures the client
type ClientConfig struct {
	LongLivedToken string
}

// Response is what we get back from PackRat
type Response struct {
	*http.Response
}

// Option is a functional option for a new packrat client
type Option func(*Client)

// WithBaseURL sets the base url for packrate in the client
func WithBaseURL(s string) Option {
	return func(c *Client) {
		c.baseURL = s
	}
}

// WithLLT sets a long lived token
func WithLLT(s string) Option {
	return func(c *Client) {
		c.llt = s
	}
}

// WithSLT sets a short lived token
func WithSLT(s string) Option {
	return func(c *Client) {
		c.slt = s
	}
}

// MustNew returns a new packrat client using functional options, or panics on error
func MustNew(opts ...Option) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new packrat client using functional options
func New(opts ...Option) (*Client, error) {
	c := &Client{
		client:    http.DefaultClient,
		userAgent: "packratking",
		baseURL:   baseURL,
		llt:       os.Getenv("IDMSOIDC_LONG_LIVED_TOKEN"),
	}
	for _, opt := range opts {
		opt(c)
	}
	if os.Getenv("PRINT_CURL") != "" {
		c.printCurl = true
	}
	if c.llt == "" && c.slt == "" {
		return nil, errors.New("must set long lived token")
	}

	if c.slt == "" {
		if err := c.getToken(); err != nil {
			return nil, err
		}
	}

	c.Location = &LocationServiceOp{client: c}
	c.Volume = &VolumeServiceOp{client: c}

	return c, nil
}

// getToken Using a long lived token, set the short token in the client
func (c *Client) getToken() error {
	tokenInfo, _, err := authenticate.ShortLivedToken(authenticate.NewShortLivedTokenConfig(
		authenticate.WithLLT(c.llt),
		authenticate.WithClientID("packrat-production"),
		authenticate.WithEndpoints(fmt.Sprintf("endpoint:%s", "/api/v2")),
	))
	if err != nil {
		return err
	}
	c.slt = tokenInfo.Token

	return nil
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	bearer := "Bearer " + c.slt
	req.Header.Add("Authorization", bearer)

	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		return nil, err
	}
	// b, _ := ioutil.ReadAll(res.Body)

	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		b, _ := io.ReadAll(res.Body)
		fmt.Fprintf(os.Stderr, "RETURN: %v\n", string(b))
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		switch {
		case res.StatusCode == http.StatusTooManyRequests:
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		case res.StatusCode == http.StatusNotFound:
			return nil, fmt.Errorf("that entry was not found, are you sure it exists?")
		default:
			return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := io.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return nil, err
		}
	} else {
		return nil, nil
	}
	r := &Response{res}

	return r, nil
}

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprint(os.Stderr, "error closing item")
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func mustMarshal(v any) []byte {
	got, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return got
}
