package packrat

// ErrorResponse is what we get back when there is an error
type ErrorResponse struct {
	Message string `json:"errors"`
}
