package packrat

import (
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	os.Clearenv()
	_, err := New()
	require.EqualError(t, err, "must set long lived token")

	// _, err = New(WithLLT("foo"))
	// require.NoError(t, err, "new client with an llt")

	_, err = New(WithSLT("foo"))
	require.NoError(t, err, "new client with an slt")
}

func testCopyFile(f string, w io.Writer) {
	rp, err := os.ReadFile(f)
	panicIfErr(err)
	_, err = io.Copy(w, bytes.NewReader(rp))
	panicIfErr(err)
}
