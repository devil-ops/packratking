package packrat

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const (
	volumesPath = "/api/v2/volumes"
)

type mountURLs struct {
	SMB []string `json:"smb,omitempty"`
	NFS []string `json:"nfs,omitempty"`
}

// Try to catch common versions of root, ro, and rw
var exportTypes = map[string][]string{
	"Root":       {"root"},
	"Read/Write": {"read/write", "read write", "r/w", "rw"},
	"Read Only":  {"read/only", "read only", "r/o", "ro"},
}

func exportTypeWithString(s string) (string, error) {
	for k, v := range exportTypes {
		for _, possible := range v {
			if strings.ToLower(s) == possible {
				return k, nil
			}
		}
	}
	return "", fmt.Errorf("no export type for: %v", s)
}

// NewExportSpec returns an export address spec using a string.
// If only the cidr is given (Example: "1.2.3.4"), 'Root' will be used as the 'type'
// For non-Root addresses, use:
// rw:1.2.3.4
// or
// ro:1.2.3.4
func NewExportSpec(s string) (*NFSExportSpec, error) {
	if !strings.Contains(s, ":") {
		return &NFSExportSpec{
			Type:    "Root",
			Address: s,
		}, nil
	}
	pieces := strings.Split(s, ":")
	if len(pieces) > 2 {
		return nil, errors.New("must use a single :, not multiples")
	}

	et, err := exportTypeWithString(pieces[0])
	if err != nil {
		return nil, err
	}

	return &NFSExportSpec{
		Type:    et,
		Address: pieces[1],
	}, nil
}

// NFSExportSpecs is multiple NFSExportSpec items
type NFSExportSpecs []NFSExportSpec

// Volume is a share from the NAS
type Volume struct {
	Active                  bool           `json:"active,omitempty" yaml:"active,omitempty"`
	APIIdentifier           string         `json:"api_identifier,omitempty" yaml:"api_identifier,omitempty"`
	Backups                 bool           `json:"backups,omitempty" yaml:"backups,omitempty"`
	BackupsDr               bool           `json:"backups_dr,omitempty" yaml:"backups_dr,omitempty"`
	BackupsOffSiteRetention float64        `json:"backups_off_site_retention,omitempty" yaml:"backups_off_site_retention,omitempty"`
	BackupsOnSiteRetention  float64        `json:"backups_on_site_retention,omitempty" yaml:"backups_on_site_retention,omitempty"`
	CreatedAt               string         `json:"created_at,omitempty" yaml:"created_at,omitempty"`
	DecommissionedAt        *string        `json:"decommissioned_at,omitempty" yaml:"decommissioned_at,omitempty"`
	DecommissionedSize      *int           `json:"decommissioned_size,omitempty" yaml:"decommissioned_size,omitempty"`
	FundCode                *string        `json:"fund_code,omitempty" yaml:"fund_code,omitempty"`
	GrouperGroup            *string        `json:"grouper_group,omitempty" yaml:"grouper_group,omitempty"`
	GrouperGroupID          *int           `json:"grouper_group_id,omitempty" yaml:"grouper_group_id,omitempty"`
	ID                      float64        `json:"id,omitempty" yaml:"id,omitempty"`
	LocationID              float64        `json:"location_id,omitempty" yaml:"location_id,omitempty"`
	MaxGB                   *int           `json:"max_gb,omitempty" yaml:"max_gb,omitempty"`
	MountURLs               mountURLs      `json:"mount_urls,omitempty" yaml:"mount_urls,omitempty"`
	Name                    string         `json:"name,omitempty" yaml:"name,omitempty"`
	Pricing                 string         `json:"pricing,omitempty" yaml:"pricing,omitempty"`
	Type                    string         `json:"type,omitempty" yaml:"type,omitempty"`
	UpdatedAt               string         `json:"updated_at,omitempty" yaml:"updated_at,omitempty"`
	URL                     string         `json:"url,omitempty" yaml:"url,omitempty"`
	UsedGB                  *int           `json:"used_gb,omitempty" yaml:"used_gb,omitempty"`
	User                    *string        `json:"user,omitempty" yaml:"user,omitempty"`
	ExportAddresses         NFSExportSpecs `json:"export_addresses,omitempty" yaml:"export_addresses,omitempty"`
}

// NFSExportSpec is the NFS export info
type NFSExportSpec struct {
	Address string `json:"address,omitempty"`
	Type    string `json:"type,omitempty"`
}

// RequestedState is what to send up in a modify request
type RequestedState struct {
	MaxGB         float64        `json:"max_gb,omitempty" yaml:"max_gb,omitempty"`
	NFSExportSpec NFSExportSpecs `json:"nfs_export_spec,omitempty" yaml:"nfs_export_spec,omitempty"`
	ShareNFS      *bool          `json:"share_nfs" yaml:"share_nfs,omitempty"`
	ShareSMB      *bool          `json:"share_smb" yaml:"share_smb,omitempty"`
}

// VolumeModifyParams are the modification parameters we send up
type VolumeModifyParams struct {
	FundCode       string         `json:"fund_code,omitempty" yaml:"fund_code,omitempty"`
	RequestedState RequestedState `json:"requested_state,omitempty" yaml:"requested_state"`
}

// injectBackupNetworks injects the backup networks to a request if the NFS exports are changing
func (v *VolumeModifyParams) injectBackupNetworks() {
	if v.RequestedState.NFSExportSpec != nil {
		v.RequestedState.NFSExportSpec = append(
			v.RequestedState.NFSExportSpec,
			NFSExportSpec{
				Type:    "Root",
				Address: "10.138.196.0/24",
			},
			NFSExportSpec{
				Type:    "Root",
				Address: "10.138.199.0/24",
			},
		)
	}
}

// VolumeCreateParams are parameters sent up with a create
type VolumeCreateParams struct {
	FundCode       string         `json:"fund_code,omitempty"`
	LocationID     float64        `json:"location_id,omitempty"`
	Name           string         `json:"name,omitempty"`
	RequestedState RequestedState `json:"requested_state,omitempty"`
}

// VolumeService is the interface describe how the volume endpoint works
type VolumeService interface {
	List(ctx context.Context) ([]Volume, *Response, error)
	Get(ctx context.Context, id string) (*Volume, *Response, error)
	GetWithID(ctx context.Context, id int) (*Volume, *Response, error)
	GetWithName(ctx context.Context, name string) (*Volume, *Response, error)
	Create(ctx context.Context, params *VolumeCreateParams) (*Volume, *Response, error)
	Modify(ctx context.Context, id float64, params *VolumeModifyParams) (*Volume, *Response, error)
	Delete(ctx context.Context, id int) (*Response, error)
}

// VolumeServiceOp is the operator for the volume service
type VolumeServiceOp struct {
	client *Client
}

// List lists out all volumes
func (svc *VolumeServiceOp) List(ctx context.Context) ([]Volume, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", svc.client.baseURL, volumesPath), nil)
	if err != nil {
		return nil, nil, err
	}
	var volumes []Volume
	resp, err := svc.client.sendRequest(req, &volumes)
	if err != nil {
		return nil, resp, err
	}
	return volumes, resp, nil
}

// Create creates a new volume
func (svc *VolumeServiceOp) Create(ctx context.Context, params *VolumeCreateParams) (*Volume, *Response, error) {
	postData, err := json.Marshal(params)
	if err != nil {
		return nil, nil, err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s%s", svc.client.baseURL, volumesPath), bytes.NewReader(postData))
	if err != nil {
		return nil, nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	var volume Volume
	_, err = svc.client.sendRequest(req, &volume)
	if err != nil {
		return nil, nil, err
	}
	return &volume, nil, nil
}

// GetWithID returns volume information using an ID
func (svc *VolumeServiceOp) GetWithID(ctx context.Context, id int) (*Volume, *Response, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s/%d", svc.client.baseURL, volumesPath, id), nil)
	if err != nil {
		return nil, nil, err
	}
	var volume Volume
	resp, err := svc.client.sendRequest(req, &volume)
	if err != nil {
		return nil, resp, err
	}
	return &volume, resp, nil
}

// Get returns volume information
func (svc *VolumeServiceOp) Get(ctx context.Context, given string) (*Volume, *Response, error) {
	id, err := strconv.Atoi(given)
	if err != nil {
		return svc.GetWithName(ctx, given)
	}
	return svc.GetWithID(ctx, id)
}

// GetWithName returns a volume using the volume name
func (svc *VolumeServiceOp) GetWithName(ctx context.Context, name string) (*Volume, *Response, error) {
	matches := []Volume{}
	allV, resp, err := svc.List(ctx)
	if err != nil {
		return nil, resp, err
	}
	for _, v := range allV {
		if v.Name == name {
			matches = append(matches, v)
		}
	}

	switch {
	case len(matches) == 1:
		return &matches[0], resp, nil
	case len(matches) > 1:
		return nil, resp, fmt.Errorf("multiple volumes found, please use id instead: %v", matches)
	default:
		return nil, resp, fmt.Errorf("no volumes found with name: %v", name)
	}
}

// Delete removes a volume
func (svc *VolumeServiceOp) Delete(ctx context.Context, id int) (*Response, error) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s%s/%d", svc.client.baseURL, volumesPath, id), nil)
	if err != nil {
		return nil, err
	}
	var nothing interface{}
	resp, err := svc.client.sendRequest(req, &nothing)
	if err != nil {
		return resp, err
	}
	return resp, nil
}

// Modify modifies a volume
func (svc *VolumeServiceOp) Modify(ctx context.Context, id float64, params *VolumeModifyParams) (*Volume, *Response, error) {
	// Remove this when it's handled upstream
	params.injectBackupNetworks()

	postData := mustMarshal(params)
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s%s/%v", svc.client.baseURL, volumesPath, id), bytes.NewReader(postData))
	if err != nil {
		return nil, nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	var volume Volume
	_, err = svc.client.sendRequest(req, &volume)
	if err != nil {
		return nil, nil, err
	}
	return &volume, nil, nil
}
