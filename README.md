# Packratking

Command line application and go library for interacting with Packrat

Clone and build, or download a binary for your os from the
[releases](https://gitlab.oit.duke.edu/devil-ops/packratking/-/releases) page

```
Interace with packrat from the command line! Be sure to get your long lived
token from the fine folks in IDMS. For information on how to do that, see:

https://packrat.oit.duke.edu/api/v2/docs

Your token can either be used with any of the methods below:

* Passed to packratking with '-l' or '--long-lived-token'
* Set in the environment variable: IDMSOIDC_LONG_LIVED_TOKEN
* Stored in ~/.packratking.yaml as 'long-lived-token'

Usage:
  packratking [command]

Available Commands:
  create      Create NFS or SMB Volumes
  delete      Delete items from Packrat
  get         Get items from packrat
  help        Help about any command
  modify      Modify object
  version     Version will output the current build information

Flags:
      --config string             config file (default is $HOME/.packratking.yaml)
  -h, --help                      help for packratking
  -l, --long-lived-token string   Long Lived Token (Can also be passed as env IDMSOIDC_LONG_LIVED_TOKEN)
  -t, --toggle                    Help message for toggle

Use "packratking [command] --help" for more information about a command.
```

### Disclaimer

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
